<?php 
require_once "includes/db.php" ;
require_once "includes/header.php" ;
require_once "includes/navigation.php" ;

$post_category_id = $_GET["category"] ;

$table = "SELECT * FROM posts WHERE post_category_id = $post_category_id" ;
$query = mysqli_query($connection,$table) ;
?>


    <!-- Page Content -->
    <div class="container">

        <div class="row">
    
       
            <!-- Blog Entries Column -->
            <div class="col-md-8">
            <?php 
        
            $i=1 ;
            while($row=mysqli_fetch_assoc($query)) :
        
        
            ?>
                <h1 class="page-header">
                    Page Heading
                    <small>Secondary Text</small>
                </h1>

                <!-- First Blog Post -->
               
                <h2>
                    <a href="post.php?p_id=<?=$row["post_id"] ?>"><?= $row["post_title"] ?></a>
                </h2>
                <p class="lead">
                    by <a href="index.php"><?= $row["post_author"] ?></a>
                </p>
                <p><span class="glyphicon glyphicon-time"></span><?= $row["post_date"] ?></p>
                <hr>
                <img class="img-responsive" src="images/<?= $row['post_image']?>" alt="">
                <hr>
                <p><?= $row["post_content"] ?>  </p>
                <a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

                <hr>
            <?php
            // if(isset($_POST["create_comment"])){
            //     $comment_author=$_POST['comment_author'];
            //     $comment_email=$_POST['comment_email'];
            //     $comment_content=$_POST['comment_content'];
            //     $comment_date = date("Y/m/d");
            //     $comment_post_id=$_POST['post_id'] ;

            //     $db_update_comment = mysqli_connect("localhost","root","","cms");
            //     $sql_update_comment = "INSERT INTO comment VALUE (
            //         '',
            //         '$comment_post_id',
            //         '$comment_author',
            //         '$comment_email',
            //         '$comment_content',
            //         'unapproved',
            //         '$comment_date'
            //     )";
            //     $query_update_comment = mysqli_query($db_update_comment,$sql_update_comment);
            // }
            ?>
            <!-- <div class="well" name="" id="">
                <h4>Leave a Comment</h4>
                    <form action="" method="post" role="form">

                        <div class="form-group">
                            <label for="aurthor">Author</label>
                            <input type="text" class="form-control" name="comment_author">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="comment_email">
                        </div>
                        <div class="form-group">
                            <label for="comments">Your Comment</label>
                            <textarea class="form-control"  rows="10" name="comment_content"></textarea>
                        </div>
                        <button type="submit"name="create_comment" class="btn btn-primary">submit</button>
                    </form>

            </div> -->
            <?php // } 
            ?>
            <?php $i++ ?>
            <?php endwhile ?>
           
               

                <!-- Pager -->
                <ul class="pager">
                    <li class="previous">
                        <a href="#">&larr; Older</a>
                    </li>
                    <li class="next">
                        <a href="#">Newer &rarr;</a>
                    </li>
                </ul>

            </div>


            <!-- Blog Sidebar Widgets Column -->
<?php 
require_once "includes/sidebar.php"
?>

        </div>
        <!-- /.row -->

        <hr>

<?php 
include_once "includes/footer.php"        
?>


