
<?php
require_once "include/header-admin.php";

$db = mysqli_connect("localhost","root","","cms") ;

if(isset($_POST["submit_post"])){
    $post_title = htmlspecialchars($_POST["title"]) ;
    $post_category_id = htmlspecialchars($_POST["post_category"]) ;
    $post_author = htmlspecialchars($_POST["author"]) ;
    $post_status = htmlspecialchars($_POST["post_status"]) ;
    $post_image = $_FILES["image"]["name"] ;
    $post_image_temp = $_FILES["image"]["tmp_name"]; 
    $post_tag = htmlspecialchars($_POST["post_tag"]) ;
    $post_content = htmlspecialchars($_POST["post_content"]) ;
    $post_date = $_POST["post_date"];
    $post_comment_count = 4;

    move_uploaded_file($post_image_temp,"../images/$post_image") ;

    $sql_add_post = "INSERT INTO posts VALUE (
        '',
        '$post_category_id',
        '$post_title',
        '$post_author',
        '$post_date',
        '$post_image',
        '$post_content',
        '$post_tag',
        '',
        '$post_status'
        )" ;

    $query_post = mysqli_query($db,$sql_add_post) ;

    if($query_post = true){
        echo "
              <script>
                   alert('Post berhasil ditambahkan !')
                   document.location.href ='add.php'
              </script>
        ";
    }else{
        echo "
        <script>
             alert('Post gagal ditambahkan !')
             document.location.href ='add.php'
        </script>
  ";
    }
}
?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
<?php
require_once "include/navigation.php";
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">

                    <h1 class="page-header">Welcome to admin
                        <small><?=$_SESSION['username'] ?></small>
                    </h1>
                     
                    <form action="" method="post" enctype="multipart/form-data">

                        <div class="form-group">
                            <label for="title">Post Title</label>
                            <input type="text" class="form-control" name="title">
                        </div>
                        <div class="form-group">
                            <label for="">Post Category</label>
                            <br>
                           <select name="post_category" id="">
                            <?php
                            $sql_select_category_id = "SELECT * FROM category" ; 
                            $query_category_id = mysqli_query($db,$sql_select_category_id) ;

                            while($row = mysqli_fetch_assoc($query_category_id)){
                                $ID = $row['ID'] ;
                                $Title = $row ['Title'] ;
                                echo "<option value='{$ID}'>{$Title}</option>" ;
                            }
                            ?>
                           </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Post Author</label>
                            <input type="text" class="form-control" name="author">
                        </div>
                        <div class="form-group">
                            <label for="">Post Status</label>
                            <br>
                           <select name="" id="" class="">
                           <option value="draft">draft</option>
                           <option value="publish">publish</option>
                           </select>
                        </div>
                        <div class="form-group">
                            <label for="post_image">Post Image</label>
                            <input type="file" name="image">
                        </div>
                        <div class="form-group">
                            <label for="post_tag">Post Tag</label>
                            <input type="text" class="form-control" name="post_tag">
                        </div>
                        <div class="form-group">
                            <label for="summernote">Post Content</label>
                            <textarea class="form-control" name="post_content" id="summernote" cols="30" rows="10"></textarea>
                        </div>
                        <div class="from-group">
                            <label for="post_date">Post Date</label>
                            <input type="date" class="form-control" name="post_date">
                        </div>
                        <br>
                        <div class="form-group">
                        <input class="btn btn-primary" type="submit" name="submit_post" value="Publish Post">
                        </div>
                    </form>

                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php 
require_once "include/footer-admin.php";
?>
