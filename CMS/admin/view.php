<?php
require_once "include/header-admin.php";

$db = mysqli_connect("localhost","root","","cms");
$sql_view_post = "SELECT * FROM posts" ;
$query_view_post = mysqli_query($db,$sql_view_post);

if(isset($_GET['delete']) > 0) {
    $id = $_GET['delete'];
    $sql_delete_post = "DELETE FROM posts WHERE post_id = $id";
    $query_delete_post = mysqli_query($db,$sql_delete_post);

    if($query_delete_post = true){
        echo
        "
        <script>
             alert('Berhasil dihapus!')
             document.location.href ='view.php'
        </script>"  ;
    }else{
       echo
       "
       <script>
            alert('Gagal dihapus !')
            document.location.href ='view.php'
       </script>";
    }
}


?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
<?php
require_once "include/navigation.php";
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">

                    <h1 class="page-header">Welcome to admin
                        <small><?=$_SESSION['username'] ?></small>
                    </h1>
                    <?php
                    if(isset($_POST['checkBoxArray'])){
                        foreach($_POST['checkBoxArray'] as $postsValueId){
                            $bulk_option = $_POST['bulk_option'];
                            switch($bulk_option){
                                
                                case 'publish':
                                    $sql_publish_status = "UPDATE posts SET post_status = '{$bulk_option}' WHERE post_id = {$postsValueId} " ;
                                    $query_publish_status = mysqli_query($db,$sql_publish_status);
                                    header("Location: view.php") ;
                                    break;
                                
                                case 'draft':
                                    $sql_draft_status = "UPDATE posts SET post_status = '{$bulk_option}' WHERE post_id = {$postsValueId} " ;
                                    $query_draft_status = mysqli_query($db,$sql_draft_status);
                                    header("Location: view.php") ;
                                    break;
                                
                                case 'delete':
                                    $sql_delete_status = "DELETE  FROM posts WHERE post_id = {$postsValueId} " ;
                                    $query_delete_status = mysqli_query($db,$sql_delete_status);
                                    header("Location: view.php") ;
                                    break;

                            }
                        }
                    }

                    ?>
                    <form action="" method="post">
                        <table class="table table-bordered table-hover">
                            <div id="bulkOptionsContainer" class="col-xs-4">
                                <select class="form-control" name="bulk_option" id="">
                                    <option value="">Select Option</option>
                                    <option value="publish">Publish</option>
                                    <option value="draft">Draft</option>
                                    <option value="delete">Delete</option>
                                </select>
                            </div>
                            <div class="col-xs-4">
                                <input type="submit" name="submit" class="btn btn-succsess" value="Apply">
                                <a class="btn btn-primary" href="add.php">Add New</a>
                            </div>
                            <thead>
                                <tr>
                                    <th><Input id="select_All_box" type="checkbox"></Input></th>
                                    <th>Id</th>
                                     <th>Author</th>
                                     <th>Title</th>
                                     <th>Category</th>
                                     <th>Status</th>
                                     <th>Image</th>
                                     <th>Tags</th>
                                     <th>Comments</th>
                                     <th>Date</th>
                                     <th>View Post</th>
                                     <th>Edit</th>
                                     <th>Delete</th>
                                </tr>
                            </thead>
                                <?php
                                $i=1;
                                while($row=mysqli_fetch_assoc($query_view_post)) :
                                ?>
                            <tbody>
                                <tr>
                                    <td><Input class="checkBoxes" type="checkbox" name="checkBoxArray[]" value="<?=$row['post_id'] ?>"></Input></td>
                                    <td><?= $row["post_id"] ?></td>
                                     <td><?= $row["post_author"]?></td>
                                     <td><?= $row["post_title"] ?></td>
                                    <?php
                                    $id = $row["post_category_id"] ;

                                    $sql_select_category_form_id = "SELECT * FROM category WHERE ID = $id" ;
                                    $query_select_category_form_id = mysqli_query($db,$sql_select_category_form_id);
                                    $row_select_category_form_id = mysqli_fetch_assoc($query_select_category_form_id) ;
                                    ?>
                                     <td><?= $row_select_category_form_id["Title"]?></td>
                                     <td><?=$row['post_status'] ?></td>
                                     <td><img width='100' src='../images/<?=$row["post_image"]?>'></td>
                                     <td><?= $row["post_tag"]?></td>
                                     <td>Comments</td>
                                     <td><?= $row["post_date"] ?></td>
                                     <td><a href="../post.php?p_id=<?=$row['post_id']?>">View Post</a></td>
                                     <td><a href="edit.php?edit=<?=$row['post_id']?>" onclick="return confirm('Yakin?');">edit</a></td>
                                     <td><a href="view.php?delete=<?=$row['post_id']?>" onclick="return confirm('Yakin?');">Delete</a></td>
                                     <?php 
                                     $i++ ;
                                     endwhile;
                                     ?>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php 
require_once "include/footer-admin.php";
?>
