
<?php
require_once "include/header-admin.php";

$db = mysqli_connect("localhost","root","","cms") ;

$id_update=$_GET["edit"] ;
$sql_select_post = "SELECT * FROM posts WHERE post_id = $id_update";
$query_select_post = mysqli_query($db,$sql_select_post) ;
$row_update_post = mysqli_fetch_assoc($query_select_post) ;


if(isset($_POST["submit_post"])){
   
    $post_category_id = $row_edit['post_category_id'] ;
    $post_title = htmlspecialchars($_POST['post_title']) ;
    $post_author = htmlspecialchars($_POST['post_author']) ;
    $post_status = htmlspecialchars($_POST['post_status']) ;
    $post_image = htmlspecialchars($_POST['post_image']) ;
    $post_tag = htmlspecialchars($_POST['post_tag']) ;
    $post_content = htmlspecialchars($_POST['post_content']) ;
    $post_date = htmlspecialchars($_POST['post_date']) ;

    $sql_update_post = "UPDATE posts SET
                        post_category_id = '$post_category_id',
                        post_title= '$post_title',
                        post_author= '$post_author',
                        post_date = '$post_date',
                        post_image = '$post_image',
                        post_content = '$post_content',
                        post_tag = '$post_tag',
                        post_status = '$post_status'
                        WHERE post_id = $id_update
                        " ;
    $query_update_post = mysqli_query($db,$sql_update_post) ;

    if($query_update_post = true) {
        echo 
        "<script>
            alert('Data Berhasil diubah !')
            document.location.href ='view.php'
        </script> ";
    } else{
        echo 
         "<script>
             alert('Data Gagal diubah !')
             document.location.href ='view.php'
         </script>";
    }

}
?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
<?php
require_once "include/navigation.php";
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">

                    <h1 class="page-header">Welcome to admin
                        <small>Author</small>
                    </h1>
                     
                    <form action="" method="post">

                        <div class="form-group">
                            <label for="title">Post Title</label>
                            <input type="text" class="form-control" name="post_title" value="<?= $row_update_post['post_title'] ?>">
                        </div>
                        <div class="form-group">
                            <select name="post_category" id="post_category">
                            <?php
                            //  $sql_select_catagory_update = " SELECT * FROM category  " ;
                            //  $query_select_category_update = mysqli_query($db,$sql_select_catagory_update);
                            //  while($row_update = mysqli_fetch_assoc($query_select_category_update)){
                            //      $id = $row_update['ID'] ;
                            //      $Title = $row_update['Title'];
                            //      echo "<option value='{$id}>{$Title}</option>" ;
                            //  }

                            ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Post Author</label>
                            <input type="text" class="form-control" name="post_author"  value="<?= $row_update_post['post_author'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="post_status">Post Status</label>
                            <input type="text" class="form-control" name="post_status"  value="<?= $row_update_post['post_status'] ?>">
                        </div>
                        <div class="form-group">
                            <img src="../images/<?= $row_update_post['post_image'] ?>" alt="" width="100">
                        </div>
                        <div class="form-group">
                            <label for="post_tag">Post Tag</label>
                            <input type="text" class="form-control" name="post_tag"  value="<?= $row_update_post['post_tag'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="post_content">Post Content</label>
                            <textarea class="form-control" name="post_content" id=""   cols="30" rows="10" ><?= $row_update_post['post_content'] ?></textarea>
                        </div>
                        <div class="from-group">
                            <label for="post_date">Post Date</label>
                            <input type="date" class="form-control" name="post_date"  value="<?= $row_update_post['post_date'] ?>">
                        </div>
                        <br>
                        <div class="form-group">
                        <input class="btn btn-primary" type="submit" name="submit_post" value="Update Post">
                        </div>
                    </form>

                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php 
require_once "include/footer-admin.php";
?>
