<?php
require_once "include/header-admin.php";

$db = mysqli_connect("localhost","root","","cms") ;
$sql_view_user = "SELECT * FROM users" ;
$query_view_user = mysqli_query($db,$sql_view_user);

if(isset($_GET["delete"]) > 0) {
    $id = $_GET["delete"] ;
    $sql_delete_user = "DELETE FROM users WHERE user_id = $id" ;
    $query_delet_user = mysqli_query($db,$sql_delete_user) ;

    if($query_delet_user = true){
        echo
        "
        <script>
             alert('Berhasil dihapus!')
             document.location.href ='view_user.php'
        </script>"  ;
    }else{
       echo
       "
       <script>
            alert('Gagal dihapus !')
            document.location.href ='view_user.php'
       </script>";
    }
}

if(isset($_GET['change_to_admin']) > 0) {
        $id = $_GET['change_to_admin'] ;
        $sql_change_to_admin = "UPDATE users SET user_role = 'admin' WHERE user_id = $id " ;
        $query_change_to_admin = mysqli_query($db,$sql_change_to_admin) ;
        
        if($query_change_to_admin = true){
            echo
            "
            <script>
                 alert('Berhasil diubah menjadi admin !')
                 document.location.href ='view_user.php'
            </script>"  ;
        }else{
           echo
           "
           <script>
                alert('Gagal diubah menjadi admin !')
                document.location.href ='view_user.php'
           </script>";
        }
}

if(isset($_GET['change_to_subscriber']) > 0 ) {
    $id = $_GET['change_to_subscriber'] ;
    $sql_change_to_subscriber = "UPDATE users SET user_role = 'subscriber' WHERE user_id = $id " ;
    $query_change_to_subscriber = mysqli_query($db , $sql_change_to_subscriber) ;

    if($query_change_to_subscriber = true){
        echo
        "
        <script>
             alert('Berhasil diubah menjadi subscriber !')
             document.location.href ='view_user.php'
        </script>"  ;
    }else{
       echo
       "
       <script>
            alert('Gagal diubah menjadi subscriber !')
            document.location.href ='view_user.php'
       </script>";
    }
}
?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
<?php
require_once "include/navigation.php";
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">

                    <h1 class="page-header">Welcome to admin
                        <small><?=$_SESSION['username'] ?></small>
                    </h1>
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                     <th>Username</th>
                                     <th>Password</th>
                                     <th>First Name</th>
                                     <th>Last Name</th>
                                     <th>Email</th>
                                     <th>Image</th>
                                     <th>Role</th>
                                     <th>randSalt</th>
                                </tr>
                            </thead>
                                <?php 
                                 $i=1;
                                 while($row=mysqli_fetch_assoc($query_view_user)) :
                                ?>
                            <tbody>
                                <tr>
                                    <td><?= $row["user_id"] ?></td>
                                    <td><?= $row["username"]?></td>
                                    <td><?= $row["user_password"] ?></td>
                                    <td><?=$row["user_firstname"] ?></td>
                                    <td><?= $row["user_lastname"] ?></td>
                                    <td><?=$row["user_email"]?></td>
                                    <td><img width='100' src='../images/<?=$row["user_image"]?>'></td>
                                    <td><?=$row["user_role"] ?></td>
                                    <td><?= $row["randSalt"] ?></td>
                                    <td><a href="view_user.php?change_to_admin=<?=$row['user_id'] ?>">Admin</a> </td>
                                    <td><a href="view_user.php?change_to_subscriber=<?=$row['user_id'] ?>">Subscriber</a> </td>
                                    <td><a href="edit_user.php?edit=<?=$row['user_id']?>">Edit</a> </td>
                                    <td><a href="view_user.php?delete=<?=$row['user_id']?>">Delete</a> </td>
                                     
                                    <?php 
                                    $i++ ;
                                    endwhile;
                                    ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php 
require_once "include/footer-admin.php";
?>
