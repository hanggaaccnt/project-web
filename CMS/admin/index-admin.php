<?php
require_once "include/header-admin.php";


?>
<body>
    
    <div id="wrapper">
        <!-- Navigation -->
<?php
require_once "include/navigation.php";
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome to Admin
                            <small><?=$_SESSION['username'] ?></small>
                        </h1>
                        <!-- <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Blank Page
                            </li>
                        </ol> -->
                    </div>
                </div>
                <!-- /.row -->
                       
                <!-- /.row widget -->
                
                <div class="row">
                        <?php 
                        $db = mysqli_connect("localhost","root","","cms") ;
                        $sql_widget_num_post = "SELECT * FROM posts" ;
                        $query_widget_num_post = mysqli_query($db,$sql_widget_num_post) ;
                        $row_num_post = mysqli_num_rows($query_widget_num_post) ;
                       
                        ?>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-file-text fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                    <div class='huge'><?=$row_num_post ?></div>
                                            <div>Posts</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="view.php">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <?php
                            $sql_widget_num_comment ="SELECT * FROM comment" ;
                            $query_widget_num_comment = mysqli_query($db,$sql_widget_num_comment) ;
                            $row_widget_num_comment = mysqli_num_rows($query_widget_num_comment) ;
                            ?> 
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-comments fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                        <div class='huge'><?=$row_widget_num_comment ?></div>
                                        <div>Comments</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="comment.php">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <?php
                            $sql_widget_num_users = "SELECT * FROM users" ;
                            $query_widget_num_users = mysqli_query($db,$sql_widget_num_users) ;
                            $row_widget_num_users = mysqli_num_rows($query_widget_num_users) ;
                            ?>
                            <div class="panel panel-yellow">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-user fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                        <div class='huge'><?=$row_widget_num_users ?></div>
                                            <div> Users</div>
                                        </div>
                                    </div>
                                </div><a href="view_user.php">
                                        <div class="panel-footer">
                                            <span class="pull-left">View Details</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <?php
                            $sql_widget_num_categories = "SELECT * FROM category" ;
                            $query_widget_num_categories = mysqli_query($db,$sql_widget_num_categories) ;
                            $row_widget_num_categories = mysqli_num_rows($query_widget_num_categories) ;
                            ?>
                            <div class="panel panel-red">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-list fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class='huge'><?=$row_widget_num_categories ?></div>
                                            <div>Categories</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="category-admin.php">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- /.row chart coloumn -->  
                    <?php
                    $sql_select_publish_post = "SELECT * FROM posts WHERE post_status ='publish' " ;
                    $query_select_publish_post = mysqli_query($db,$sql_select_publish_post) ;
                    $row_publish_post = mysqli_num_rows($query_select_publish_post) ;

                    $sql_select_draft_post = "SELECT * FROM posts WHERE post_status ='draft' " ;
                    $query_select_draft_post = mysqli_query($db,$sql_select_draft_post) ;
                    $row_draft_post = mysqli_num_rows($query_select_draft_post) ;
                    
                    $sql_select_comment_unapproved = "SELECT * FROM comment WHERE comment_status ='unapproved' " ;
                    $query_select_comment_unapproved = mysqli_query($db,$sql_select_comment_unapproved) ;
                    $row_unapproved_comment = mysqli_num_rows($query_select_comment_unapproved) ;
                    
                    $sql_select_user_subscriber = "SELECT * FROM users WHERE user_role ='subscriber' " ;
                    $query_select_user_subscriber = mysqli_query($db,$sql_select_user_subscriber) ;
                    $row_user_subscriber = mysqli_num_rows($query_select_user_subscriber) ;

                    ?>
                    <div class="row">
                        <script type="text/javascript">
                            google.charts.load('current', {'packages':['bar']});
                            google.charts.setOnLoadCallback(drawChart);

                            function drawChart() {
                                var data = google.visualization.arrayToDataTable([
                                ['Date', 'Count'],
                                <?php
                                $element_text = ['Active Posts', 'Publish Posts','Draft Posts','Comments', 'Pending Comments','Users', 'Subscriber','Categories'] ;
                                $element_count = [$row_num_post, $row_publish_post,$row_draft_post, $row_widget_num_comment, $row_unapproved_comment,$row_widget_num_users, $row_user_subscriber,$row_widget_num_categories] ;
                                for($i=0; $i < 8 ; $i++){
                                    echo "['{$element_text[$i]}'" . "," . "{$element_count[$i]}],";
                                }
                                ?>
                               // ['Posts', 1000]
                                ]);

                                var options = {
                                chart: {
                                    title: '',
                                    subtitle: '',
                                }
                                };

                                var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

                                chart.draw(data, google.charts.Bar.convertOptions(options));
                            }
                            
                        </script>
                    <div id="columnchart_material" style="width: 'auto'; height: 500px;"></div>

                </div>



            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php 
require_once "include/footer-admin.php";
?>
