<?php
require_once "include/header-admin.php";

$db = mysqli_connect("localhost","root","","cms");
$sql_view_all_comment = "SELECT * FROM comment" ;
$query_view_all_comment = mysqli_query($db,$sql_view_all_comment);

if(isset($_GET['delete'])){
    $id_comment = $_GET['delete'] ;
    $sql_delete_comment = "DELETE FROM comment WHERE comment_id = $id_comment";
    $query_delete_comment = mysqli_query($db,$sql_delete_comment);
    if($query_delete_post = true){
        echo
        "
        <script>
             alert('Berhasil dihapus!')
             document.location.href ='comment.php'
        </script>"  ;
    }else{
       echo
       "
       <script>
            alert('Gagal dihapus !')
            document.location.href ='comment.php'
       </script>";
    }
}

if(isset($_GET['approve'])){
    $id_approve = $_GET['approve'] ;
    $sql_approve_comment = "UPDATE comment SET comment_status = 'approved' WHERE comment_id = $id_approve ";
    $query_approve_comment = mysqli_query($db,$sql_approve_comment);
    header("Location: comment.php");
}

if(isset($_GET['unapprove'])){
    $id_unapprove = $_GET['unapprove'] ;
    $sql_unapprove_comment = "UPDATE comment SET comment_status = 'unapproved' WHERE comment_id = $id_unapprove ";
    $query_unapprove_comment = mysqli_query($db,$sql_unapprove_comment);
    header("Location: comment.php");
}

// if(isset($_GET['delete']) > 0) {
//     $id = $_GET['delete'];
//     $sql_delete_post = "DELETE FROM posts WHERE post_id = $id";
//     $query_delete_post = mysqli_query($db,$sql_delete_post);

//     if($query_delete_post = true){
//         echo
//         "
//         <script>
//              alert('Berhasil dihapus!')
//              document.location.href ='view.php'
//         </script>"  ;
//     }else{
//        echo
//        "
//        <script>
//             alert('Gagal dihapus !')
//             document.location.href ='view.php'
//        </script>";
//     }
// }


?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
<?php
require_once "include/navigation.php";
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">

                    <h1 class="page-header">Welcome to admin
                        <small><?=$_SESSION['username'] ?></small>
                    </h1>
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                     <th>Author</th>
                                     <th>Comment</th>
                                     <th>Email</th>
                                     <th>Status</th>
                                     <th>In Respone To</th>
                                     <th>Date</th>
                                     <th>Approve</th>
                                     <th>Unapprove</th>
                                     <th>Delete</th>
                                </tr>
                            </thead>
                                <?php
                                $i=1;
                                while($row=mysqli_fetch_assoc($query_view_all_comment)) :
                                ?>
                            <tbody>
                                <tr>
                                    <td><?= $row["comment_id"] ?></td>
                                    <td><?= $row["comment_author"]?></td>
                                    <td><?= $row["comment_content"] ?></td>                           
                                    <td><?= $row["comment_email"]?></td>
                                    <td><?=$row['comment_status'] ?></td>
                                    <?php
                                    $id=$row['comment_post_id'];                                   
                                    $sql_respon_to = "SELECT * FROM posts WHERE post_id = $id";
                                    $query_respon_to = mysqli_query($db,$sql_respon_to);
                                    while($row_respon_to = mysqli_fetch_assoc($query_respon_to)):
                                    ?>
                                    <td><a href="../post.php?p_id=<?=$row_respon_to['post_id']?>"><?=$row_respon_to['post_title']; ?></a></td>
                                    <?php endwhile ?>
                                    <td><?= $row["comment_date"]?></td>
                                   
                                    <td><a href="comment.php?approve=<?=$row["comment_id"] ?>">Approve</a></td>
                                    <td><a href="comment.php?unapprove=<?=$row["comment_id"] ?>">Unapprove</a></td>                                   
                                    <td><a href="comment.php?delete=<?=$row['comment_id']?>" onclick="return confirm('Yakin?');">Delete</a></td>
                                    <?php 
                                    $i++ ;
                                    endwhile;
                                    ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php 
require_once "include/footer-admin.php";
?>
