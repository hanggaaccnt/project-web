<?php
require_once "db.php" ;
session_start();

$db = mysqli_connect("localhost","root","","cms") ;

if(isset($_POST["login"])) {
    $username=$_POST['username'];
    $password=$_POST['password'];
    //menghindari sql injection
    $username=mysqli_real_escape_string($db,$username);
    $password=mysqli_real_escape_string($db,$password);

    $sql_login_username = "SELECT * FROM users WHERE username = '{$username}' " ;
    $query_login_username = mysqli_query($db,$sql_login_username) ;
    
    if(!$query_login_username){
        die("QUERY FAILED " . mysqli_error($db)) ;
    }

    while($row=mysqli_fetch_array($query_login_username)){
        $db_id = $row['user_id'] ;
        $db_username = $row['username'] ;
        $db_user_password = $row['user_password'] ;
        $db_user_firstname = $row['user_firstname'] ;
        $db_user_lastname = $row['user_lastname'] ;
        $db_user_role = $row['user_role'] ;
    }

    if($username !== $db_username && $password !== $db_user_password){
        header("Location: ../index.php") ;
    } else if ($username === $db_username && $password === $db_user_password){
        $_SESSION['username'] = $db_username ;
        $_SESSION['firstname'] = $db_user_firstname ;
        $_SESSION['lastname'] = $db_user_lastname ;
        $_SESSION['role'] = $db_user_role ;

        header("Location: ../admin/index-admin.php") ;
    }else{
        header("Location: ../index.php") ;
    }
}